using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ScriptableObjects;

public class ItemController : MonoBehaviour
{
    public static ItemController itemController;

    public Inventory playerInventory;
    public SlotController currentController;
    public ItemData currentItem;
    private GameObject _item;
    public Image[] images;
    private int inventoryIndex = 0;
    public GameObject slot;
    void Awake()
    {
        playerInventory.VaciarInventario();
        if (itemController != null && itemController != this)
        {
            Destroy(this);
        }
        else
        {
            itemController = this;
        }
    }
    private void Update()
    {
        ChangeCurrentWeapon();
        if(currentController!=null)
            currentController.OnUpdate();
    }

    public void AddItem(ItemData itemtoAdd)
    {
        bool trobat = false;
        for (int i = 0; i < playerInventory.slots.Length; i++)
        {
            try
            {
                if (playerInventory.slots[i] == itemtoAdd && itemtoAdd.GetType() == typeof(WeaponDataSO))
                {
                    var weapon = (WeaponDataSO)playerInventory.slots[i];
                    weapon.isIniatilize();
                    trobat = true;
                    break;
                }
            }
            catch {}
        }
        if (!trobat)
        {
            if (itemtoAdd.GetType() == typeof(WeaponDataSO))
            {
                var arma = (WeaponDataSO)itemtoAdd;
                playerInventory.AddItem(arma);
                arma.isIniatilize();
            }
            else
            {
                playerInventory.AddItem(itemtoAdd);
            }
        }
       
    }
    public void UpdateUIWeapons()
    {
        for (int i = 0; i < playerInventory.slots.Length; i++)
        {
            if (playerInventory.slots[i] != null)
            {
                images[i].sprite = playerInventory.slots[i].DefaultItemSprite;
            }
        }
    }
    public void ChangeCurrentWeapon()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            inventoryIndex = 0;
            currentItem = playerInventory.slots[inventoryIndex];
            CheckItemType();
            EquipWeapon();
        }
    }
    public void EquipWeapon()
    {
        if (_item != null)
            Destroy(_item);
        _item = Instantiate(currentItem.prefabObject, slot.transform.position, slot.transform.rotation);
        _item.transform.parent = slot.transform.parent;
    }
    public void CheckItemType()
    {
        if (currentItem != null)
        {
            if (currentItem.GetType() == typeof(WeaponDataSO))
            {

                currentController = GetComponent<Weapon>();
                var asd = (Weapon)currentController;
                asd.weaponData = (WeaponDataSO)currentItem;
                asd.OnAwake();
            }
        }
    }
}
