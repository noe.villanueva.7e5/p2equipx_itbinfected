using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour
{
    public Slider healthSlider;
    public bool isHit = false;
    public float health;
    public float maxHealth;
    public GameObject imageBlood;
    // Start is called before the first frame update
    void Start()
    {
        health = maxHealth;
        SetHealthUI();
    }

    public void DealDamage(float damage)
    {
        health -= damage;
        isHit = true;
        SetHealthUI();
        CheckDeath();
        StartCoroutine(WaitForImageBlood());    
    }
    public void HealCharacter(float heal)
    {
        health += heal;
        CheckOverheal();
        CheckDeath();
        SetHealthUI();
    }
    private void SetHealthUI()
    {
        healthSlider.value = CalculateHealthPersentage();
    }
    private void CheckOverheal()
    {
        if (health > maxHealth)
        {
            health = maxHealth;
        }
    }
    private void CheckDeath()
    {
        if (health < 0)
        {
            health = 0;
           // Destroy(this.gameObject);
            SceneManager.LoadScene(2);
        }
    }
    float CalculateHealthPersentage()
    {
        isHit = false;
        return health / maxHealth;
    }
    IEnumerator WaitForImageBlood()
    {
        imageBlood.SetActive(true);
        yield return new WaitForSeconds(2f);
        imageBlood.SetActive(false);
    }
}