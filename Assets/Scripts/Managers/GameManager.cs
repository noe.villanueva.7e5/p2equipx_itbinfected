using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    public GameObject ItemPanel;
    public GameObject porta;
    public bool jocFinalitzat = false;
    private float temps = 0;
    public static float barraProgress = 0;
    private int minuts, segons, cents;
    
    public static float finalTime;
    public static int finalKills;
    public static int KillsAmount = 0;
    private bool isDoorOpened = false;

    public GameObject pausePanel;
    public GameObject settingsPanel;
    public GameObject player;
    public bool isPaused = false;
    private bool cursorLocked = false;
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    private void Update()

    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused)
             {
                 Resume();
             }
             else
             {
                 Pause();
             }
            
        }

        if (cursorLocked && Input.GetMouseButtonDown(0))
        {
            Cursor.lockState = CursorLockMode.Locked;
        }

        temps += Time.deltaTime;

        if(barraProgress >= 120f && !isDoorOpened)
        {
            //Obrir porta
            ObrirPorta();
            jocFinalitzat = true;
            isDoorOpened = true;
        }

    }

    public static void ActivateFinalRound()
    {
        barraProgress += Time.deltaTime;
    }

    public void ApplicationQuit()
    {
        Application.Quit();
    }
    public void GoToMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void TancarPorta()
    {
        StartCoroutine(CloseDoor());
    }

    public void ObrirPorta()
    {
        Debug.Log("Pots sortir");
        StartCoroutine(OpenDoor());
    }

    void Resume()
    {
        pausePanel.SetActive(false);
        player.SetActive(true);
        Time.timeScale = 1f;
        isPaused = false;
    }

    void Pause()
    {
        pausePanel.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;
        player.SetActive(false);
        Cursor.lockState = CursorLockMode.None;
        cursorLocked = false;

    }

    public void OnClickResumeButton()
    {
        pausePanel.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;
        player.SetActive(true);
        Cursor.lockState = CursorLockMode.Locked;
        cursorLocked = true;
    }
    
    public void OnClickSettingsButton()
    {
        settingsPanel.SetActive(true);
    }
    public void ExitGame()
    {
        Application.Quit();
    }
    IEnumerator CloseDoor()
    {
        yield return new WaitForSeconds(1f);
        Debug.Log("Tanca porta");

        /*Quaternion rotationVector = transform.rotation;

        Quaternion targetVector = Quaternion.Euler(rotationVector.eulerAngles.x, rotationVector.eulerAngles.y, 0f);

        porta.transform.rotation = targetVector;*/
        porta.GetComponent<Animation>().Play();
    }

    IEnumerator OpenDoor()
    {
        yield return new WaitForSeconds(1.5f);

        porta.GetComponent<MeshCollider>().enabled = false;
        //Quaternion rotationVector = transform.rotation;
        //Quaternion targetVector = Quaternion.Euler(rotationVector.eulerAngles.x, rotationVector.eulerAngles.y, 90f);
        
        //Quaternion targetVector = Quaternion.Euler(0f, 0f, 90f);

        //porta.transform.rotation = targetVector;
    }

    IEnumerator Final()
    {
        yield return new WaitForSeconds(1.5f);
        Cursor.lockState = CursorLockMode.None;

        finalTime = temps;
        finalKills = KillsAmount;

        SceneManager.LoadScene(2);
    }

    public void CarregarEscenaFinal()
    {
        StartCoroutine(Final());
    }
}

