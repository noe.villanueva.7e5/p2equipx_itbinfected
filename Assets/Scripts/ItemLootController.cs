using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ScriptableObjects;
using System;

public class ItemLootController : MonoBehaviour
{
    public Inventory inventory;
    public ItemData arma;
    public GameObject Panel;
    public GameObject PanelBullets;

    private void Start()
    {
        PanelBullets = GameObject.Find("BulletPanel");
        PanelBullets.SetActive(false);
    }

    private void OnTriggerStay(Collider collision)
    {
        if (collision.tag == "Player")
        {
            Panel.SetActive(true);
            if (Input.GetKey(KeyCode.E))
            {
                PanelBullets.SetActive(true);
                ItemController.itemController.AddItem(arma);
                ItemController.itemController.UpdateUIWeapons();
                Destroy(gameObject);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        Panel.SetActive(false);
    }
}