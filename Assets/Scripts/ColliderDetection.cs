using UnityEngine;

public class ColliderDetection : MonoBehaviour
{
    private GameManager gameManager;
    private bool jocFinalitzat;

    private void Start()
    {
        gameManager = GameObject.Find("Managers").GetComponent<GameManager>();
        jocFinalitzat = gameManager.jocFinalitzat;
    }

    private void Update()
    {
        jocFinalitzat = gameManager.jocFinalitzat;
    }

    private void OnTriggerExit(Collider other)
    {
       if (other.CompareTag("Player") && jocFinalitzat == true)
       {
           Debug.Log("Po joc finalitzat");
           gameManager.CarregarEscenaFinal();
       }
       else if (other.CompareTag("Player"))
       {
           gameManager.TancarPorta();
       }
    }


}