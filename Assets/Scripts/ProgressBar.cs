using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    public float progresoMaximo = 120f; 
    //public RectTransform imagen;
    public Text progresText;
    public float tiempoEntreCambios = 0.1f; 
    private float tiempoAcumulado = 0f; 
    private bool progresoCompleto = false;
    public void BarraProgres()
    {
        if (!progresoCompleto)
        {
          
            tiempoAcumulado += Time.deltaTime;

  
            float progresoActual = tiempoAcumulado / progresoMaximo;

            if (progresoActual >= 1f)
            {
                progresoCompleto = true;
                progresoActual = 1f;
            }


            /*float nuevaAnchura = Mathf.Lerp(0f, 844.6f, progresoActual);
            float nuevaPosicionX = Mathf.Lerp(40f, 452.059601f, progresoActual);
            imagen.position = new Vector3(nuevaPosicionX, imagen.position.y, imagen.position.z);
            imagen.sizeDelta = new Vector2(nuevaAnchura, imagen.sizeDelta.y);*/
            int progresoEnPorcentaje = Mathf.RoundToInt(progresoActual * 100f);
            progresText.text = progresoEnPorcentaje + "%";
        }
    }
    private void Update()
    {
        BarraProgres();
    }
}