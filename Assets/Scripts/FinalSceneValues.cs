using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinalSceneValues : MonoBehaviour
{
    public Text timer;
    public Text textKills;

    void Start()
    {
        float finalTime = GameManager.finalTime;
        int finalKills = GameManager.KillsAmount;

        timer.text = FormatTime(finalTime);
        textKills.text = Kills(finalKills);
    }

    private string FormatTime(float time)
    {
        
        int minuts = (int)(time / 60f);
        int segons = (int)(time - minuts * 60f);
        int cents = (int)((time - (int)time) * 100f);

        return string.Format("{0:00} : {1:00} : {2:00}", minuts, segons, cents);
    }

    private string Kills(int kills)
    {
        return kills.ToString();
    }
}
