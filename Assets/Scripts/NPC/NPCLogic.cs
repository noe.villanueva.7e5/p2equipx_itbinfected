using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using TMPro;
using StarterAssets;
using ScriptableObjects;

public class NPCLogic : MonoBehaviour
{
    public Inventory playerInventory;
    public GameObject questionSymbol;
    public Weapon weapon;
    public FirstPersonController player;
    public GameObject InteractionPanel;
    public GameObject MissionPanel;
    public GameObject TasksPanel;
    public Text misionText;
    public int misionActual = 0;
    public bool acceptMision;
    public static bool closeToMe;
    public Animator animator;
    public NavMeshAgent navMeshAgent;
    public Vector3 destination = new Vector3(95.612f, -2.481f, 5.321f);
    
    //Variables hackeo
    public bool Hacking = false;
    public GameObject progressBar;

    void Start()
    {
        player = GameObject.Find("Player").GetComponent<FirstPersonController>();

    }

    void Update()
    {
        if (navMeshAgent.GetComponent<Transform>().position == new Vector3(95.612f, -2.070306f, 5.166667f))
        {
            Hacking = true;
            progressBar.SetActive(true);
            GameManager.ActivateFinalRound();
        }
        if (acceptMision == false) questionSymbol.SetActive(true);
        if (Input.GetKeyDown(KeyCode.E) && acceptMision == false && closeToMe == true)
        {   
            if(misionActual > 4)
            {
                MissionPanel.GetComponentInChildren<Text>().text = "Ja ho tinc tot preparat, SOM-HI AMB EL HACKEIG!";
            }
            else
            {
                MissionPanel.GetComponentInChildren<Text>().text = MissionController.Instance.AllMissions.inventory[misionActual].MissionPanelText;
            }

            Vector3 playerPosition = new Vector3(transform.position.x, player.gameObject.transform.position.y, transform.position.z);
            player.gameObject.transform.LookAt(playerPosition);

            StopGame();
            InteractionPanel.SetActive(false);
            MissionPanel.SetActive(true);
            Cursor.visible = true;
        }
    }

    private void OnEnable()
    {
        MissionItem.OnFound += MissionCompleted;
    }
    private void OnDisable()
    {
        
    }

    private void MissionCompleted()
    {
        acceptMision = false;
        misionText.color = Color.green;
    }

    private void OnTriggerEnter(Collider other)
    {
        closeToMe = true;
        if (acceptMision == false) InteractionPanel.SetActive(true);
    }

    private void OnTriggerExit(Collider other)
    {
        closeToMe = false;
        MissionPanel.SetActive(false);
        InteractionPanel.SetActive(false);
    }
    
    public void Accept()
    {
        Cursor.visible = false;
        if (misionActual > 4)
        {
            TasksPanel.SetActive(false);
            //Intentar rotar el NPC al caminar
            animator.SetTrigger("NPCWalk");
            navMeshAgent.destination = destination;
            
        }
        else
        {
            for (int i = 0; i < playerInventory.slots.Length; i++)
            {
                if (playerInventory.slots[i] != null)
                {
                    if (playerInventory.slots[i].GetType() == typeof(WeaponDataSO))
                    {
                        var pistol = (WeaponDataSO)playerInventory.slots[i];
                        pistol.isIniatilize();
                    }
                }
            }
            Debug.Log("Estic");
            MissionController.Instance.AddMision(misionActual);
            misionText.text = MissionController.Instance.ActiveMissions.inventory[misionActual++].TasksPanelText;
            misionText.color = Color.white;
            TasksPanel.SetActive(true);
        }

        acceptMision = true;
        RestartGame();

        questionSymbol.SetActive(false);
        MissionPanel.SetActive(false);
        InteractionPanel.SetActive(false);
    }

    public void Reject()
    {
        Cursor.visible = false;
        RestartGame();
        MissionPanel.SetActive(false);
        InteractionPanel.SetActive(true);
    }

    public void StopGame()
    {
        player.enabled = false;
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0;
    }

    public void RestartGame()
    {
        player.enabled = true;
        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = 1;
    }
}
