using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MissionItem : MonoBehaviour
{
    public GameObject ItemPanel;
    public AudioSource audioSource;
    public AudioClip clip;
    public bool closeToMe;
    public static event Action OnFound = delegate{};

    void Start()
    {
        ItemPanel = GameManager.Instance.ItemPanel;
        Debug.Log(ItemPanel);
        audioSource = GetComponentInParent<AudioSource>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && closeToMe == true)
        {
            Debug.Log("Input recibido");
            StartCoroutine(WaitForSound());
            
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            ItemPanel.SetActive(true);
            closeToMe = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        ItemPanel.SetActive(false);
        closeToMe = false;
    }

    /*void Collect()
    {
        audioSource.clip = collectSound;
       
        //audioSource.PlayOneShot(collectSound);
    }*/
    IEnumerator WaitForSound()
    {
        audioSource.clip = clip;
        audioSource.Play();
        yield return new WaitForSeconds(0.25f);
        Destroy(this.gameObject);
        OnFound.Invoke();
        ItemPanel.SetActive(false);
    }
}
