using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManagerClips : MonoBehaviour
{
    public static AudioManagerClips AudioManager;
    public ScriptableAudios audioListSO;
    public AudioMixer audioMixer;
    public AudioMixerSnapshot AudioSnapGame;
    public AudioMixerSnapshot AudioSnapMenu;

    private void Awake()
    {
        if (AudioManager != null)
            Destroy(this.gameObject);
        AudioManager = this;

    }
    public void OnMenu()
    {
        AudioSnapMenu.TransitionTo(0.1f);
    }
    public void OnGame()
    {
        AudioSnapGame.TransitionTo(0.1f);
    }
}
