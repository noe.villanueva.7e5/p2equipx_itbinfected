using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Sounds", menuName = "ScriptableObjects/Sounds", order = 1)]

public class ScriptableAudios : ScriptableObject
{

    public AudioClip walkPlayer;
    public AudioClip runPlayer;
    public AudioClip rechargePistol;
    public AudioClip shootPistolPlayer;
    public AudioClip patrolZombie;
    public AudioClip attackZombie;
    public AudioClip dieZombie;
}
