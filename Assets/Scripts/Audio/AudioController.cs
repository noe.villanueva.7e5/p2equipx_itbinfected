using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    private AudioSource _audioSource;
    private bool isWalking = false;
    private bool isRunning = false;
    public AudioClip walkPlayer;
    public AudioClip runPlayer;
    public ItemData currentItem;

    void Start()
     {
         _audioSource = GetComponent<AudioSource>();

     }

     // Update is called once per frame
     void Update()
     {
         if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.W))
         {
             // Reproduce el AudioClip si no est� sonando
             if (!isWalking)
             {
                 _audioSource.clip= AudioManagerClips.AudioManager.audioListSO.walkPlayer ;
                 _audioSource.Play();
                 isWalking = true;
             }
         }
         else
         {
             // Detiene el AudioClip si est� sonando
             if (isWalking)
             {
                 _audioSource.clip = AudioManagerClips.AudioManager.audioListSO.walkPlayer;
                 _audioSource.Stop();
                 isWalking = false;
             }
         }

         if (Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift))
         {
            
             // Cambia el AudioClip a reproducir
            /*_audioSource.clip = AudioManagerClips.AudioManager.audioListSO.walkPlayer;
            _audioSource.Stop();
            _audioSource.clip = AudioManagerClips.AudioManager.audioListSO.runPlayer;
            _audioSource.Play();*/
         }
        /*if (ItemController.itemController.currentItem !=null)
        {
            currentItem = ItemController.itemController.currentItem;
        }
      
        if (currentItem != null)
        {
            if (currentItem.GetType() == typeof(WeaponDataSO) && Input.GetMouseButtonDown(0))
            {
                _audioSource.clip = AudioManagerClips.AudioManager.audioListSO.shootPistolPlayer;
                _audioSource.Play();
            }
        }
        

        if (Input.GetKey(KeyCode.R))
        {
            _audioSource.clip = AudioManagerClips.AudioManager.audioListSO.rechargePistol;
            _audioSource.Play();
        }*/
     }
}
