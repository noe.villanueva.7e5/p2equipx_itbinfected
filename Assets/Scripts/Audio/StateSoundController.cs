using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateSoundController : MonoBehaviour
{
    public AudioClip patrollingSound;
    public AudioClip chasingSound; 
    public AudioClip attackingSound; 

    private AudioSource audioSource; 
    private Animator animator; 

    private void Start()
    {
        animator = GetComponent<Animator>(); 

        
        audioSource = GetComponent<AudioSource>();
        if (audioSource == null)
        {
            audioSource = gameObject.AddComponent<AudioSource>();
        }
    }

    private void Update()
    {
        if (animator.GetBool("isPatrolling") ==  true)
        {
            PlaySound(patrollingSound);
        }
        else if (animator.GetBool("isChasing") == true)
        {
            PlaySound(chasingSound);
        }
        else if (animator.GetBool("isAttacking") == true)
        {
            PlaySound(attackingSound);
        }
        else
        {
            StopSound();
        }
    }

    private void PlaySound(AudioClip clip)
    {
        
        if (!audioSource.isPlaying)
        {
            audioSource.clip = clip;
            audioSource.Play();
        }
    }

    private void StopSound()
    {
       
        if (audioSource.isPlaying)
        {
            audioSource.Stop();
        }
    }
}
