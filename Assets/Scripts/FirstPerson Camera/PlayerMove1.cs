using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerMove1 : MonoBehaviour
{
    [SerializeField]
    private float currentSpeed = 0f;
    [SerializeField]
    private float playerSpeed = 4.0f;
    [SerializeField]
    private float runSpeed = 6.0f;
    [SerializeField]
    private float jumpHeight = 1f;
    [SerializeField]
    private float gravityValue = -9.81f;
    private CharacterController controller;
    private Vector3 playerVelocity;
    private bool groundedPlayer;
    private InputManager _inputManager;
    private Transform _cameraTransform;
    private Animator _playerAnimator;
    private bool _isCrouching = false;
    private bool _isRunning = false;
    private bool _hitted = false;

    private void Start()
    {
        controller = GetComponent<CharacterController>();
        _inputManager = InputManager.Instance;
        _cameraTransform = Camera.main.transform;
        _playerAnimator = GetComponent<Animator>();
    }
   
    void Update()
    {
        transform.rotation = _cameraTransform.rotation;
        groundedPlayer = controller.isGrounded;
        if (groundedPlayer && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;
        }
        if (_inputManager.PlayerRun())
        {
            Debug.Log("RUN");
            currentSpeed = runSpeed;
        }
        else currentSpeed = playerSpeed;
        Vector2 movement = _inputManager.GetPlayerMovement();
        Vector3 move = new Vector3(movement.x, 0f, movement.y);
        move = _cameraTransform.forward * move.z + _cameraTransform.right * move.x;
        move.y = 0f;
        controller.Move(move * Time.deltaTime * currentSpeed);

        // Changes the height position of the player..
        if (_inputManager.PlayerJump() && groundedPlayer)
        {
            playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
        }
        if (_inputManager.PlayerCrouch())
        {
            _playerAnimator.SetBool("isCrouching", true);
            _isCrouching = true;
        }
        else
        {
            _playerAnimator.SetBool("isCrouching", false);
            _isCrouching = false;
        }
        if (_isCrouching)
        {
            move = Vector3.zero;
        }
      
        playerVelocity.y += gravityValue * Time.deltaTime;
        _playerAnimator.SetFloat("speed", new Vector2(move.x,move.z).magnitude*currentSpeed);
        controller.Move(playerVelocity * Time.deltaTime);
        
    }
    public void Hitted()
    {
        _hitted = true;

    }

    public void EndHitted()
    {
        _hitted = false;
    }
}