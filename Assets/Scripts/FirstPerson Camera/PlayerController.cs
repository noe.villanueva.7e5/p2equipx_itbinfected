using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;

[RequireComponent(typeof(CharacterController))]

public class PlayerController : MonoBehaviour
{
	private CharacterController controller;
      private bool groundedPlayer;
      private InputManager inputManager;
      private Transform cameraTransform;
    [SerializeField] new Camera camera;


    [SerializeField]
      private float playerSpeed = 2.0f;
      [SerializeField]
      private float jumpHeight = 1.0f;
      [SerializeField]
      private float gravityValue = -9.81f;
    [SerializeField] bool cursorLock = true;

    [SerializeField] [Range(0.0f, 1.5f)] float mouseSmoothTime = 0.05f;
    private Vector3 move;
    private InputAction action;
    private InputAction action2;

    Vector2 currentMouseDelta;
    Vector2 currentMouseDeltaVelocity;
    float cameraCap;
    [SerializeField] float mouseSensitivity = 2.5f;

     public Animator Animator;
    private void Awake()
    {
        action = GetComponent<PlayerInput>().actions["Movement"];
        action2 = GetComponent<PlayerInput>().actions["Look"];
    }
    private void OnEnable()
    {
        move = Vector3.zero;
        
        action.performed += ctx => Move(ctx);
        action2.performed += ctx => Mouse(ctx);
    }
    private void OnDisable()
    {
        action.performed -= ctx => Move(ctx);
        action2.performed -= ctx => Mouse(ctx);
    }





    private void Start()
      {

          controller = GetComponent<CharacterController>();
          inputManager = InputManager.Instance;
          cameraTransform = Camera.main.transform;
          Animator = GetComponent<Animator>();


        if (cursorLock)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = true;
        }

    }
    public void Move(InputAction.CallbackContext ctx)
    {
        Vector2 movement =action.ReadValue<Vector2>();
        move = new Vector3(movement.x, 0f, movement.y);
        move = cameraTransform.forward * move.z + cameraTransform.right * move.x;
        move.y = 0;
    }

    public void Mouse(InputAction.CallbackContext ctx)
    {
     
        //Vector2 targetMouseDelta = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        Vector2 targetMouseDelta = action2.ReadValue<Vector2>();

        currentMouseDelta = Vector2.SmoothDamp(currentMouseDelta, targetMouseDelta, ref currentMouseDeltaVelocity, mouseSmoothTime);

        cameraCap -= currentMouseDelta.y * mouseSensitivity;
        cameraCap += currentMouseDelta.x * mouseSensitivity;


        //cameraCap = Mathf.Clamp(cameraCap, -90.0f, 90.0f);

        cameraTransform.localEulerAngles = Vector3.right * cameraCap;
        transform.Rotate(Vector3.up * currentMouseDelta.x * mouseSensitivity);
        //transform.Rotate(transform.right * currentMouseDelta.y * mouseSensitivity);

        //transform.Rotate(Vector3.up * currentMouseDelta.y * mouseSensitivity);
    }
    void Update()
    {
        if (action.ReadValue<Vector2>().magnitude < 0.1f) move = Vector3.zero;
       // transform.rotation = camera.transform.rotation;
        groundedPlayer = controller.isGrounded;
        if (groundedPlayer && move.y < 0)
        {
            move.y = 0f;
        }

        
        controller.Move(move * Time.deltaTime * playerSpeed);

        /* if (move != Vector3.zero)
        {
          gameObject.transform.forward = move;

       }*/

        // Changes the height position of the player..
        if (inputManager.PlayerJump() && groundedPlayer)
        {
            move.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
        }

        move.y += gravityValue * Time.deltaTime;
        controller.Move(move * Time.deltaTime);
        controller.Move(new Vector3(move.x * playerSpeed, move.y, move.z * playerSpeed)*Time.deltaTime);

        if (groundedPlayer! && move.y < -1f)
        {
            move.y = -8f;
        }
        Animator.SetFloat("speed", new Vector2(move.x, move.z).magnitude*playerSpeed);
        Debug.Log(new Vector2(move.x, move.z).magnitude * playerSpeed);
    }
}
