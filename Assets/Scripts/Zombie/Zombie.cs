using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Zombie : MonoBehaviour
{
    public int maxHealth = 100;
    public int currentHealth;
    private Animator _animator;
    private NavMeshAgent navMeshAgent;

    void Start()
    {
        currentHealth = maxHealth;
        navMeshAgent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();
    }
    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        if (currentHealth <= 0)
        {
            StartCoroutine(WaitforAnimation());
        }
        else
        {
            _animator.SetTrigger("damage");
        }
    }

    void Die()
    {
        GameManager.KillsAmount += 1;
        Destroy(gameObject);
    }
    IEnumerator WaitforAnimation()
    {
        GetComponent<Collider>().enabled = false;
        _animator.SetTrigger("die");
        navMeshAgent.isStopped = true;
        yield return new WaitForSeconds(3.5f);
        Die();
    }
}
