using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{


    public GameObject zombiePrefab;
    public GameObject Player;
    //public Transform[] spawnPoints;
    public List<Collider> spawnColliders;
    public int maxZombies = 5; 
    public float spawnCooldown = 5f; 

    private int currentZombies; 
    private bool canSpawn; 

    private void Start()
    {
        canSpawn = false;
        currentZombies = 0;
        StartCoroutine(WaitForPlayerEnter());
    }

    private IEnumerator WaitForPlayerEnter()
    {
        while (!PlayerIsInsideAnyCollider()) 
        {
            yield return null;
        }
        canSpawn = true;
        StartCoroutine(SpawnZombies());
    }

    private bool PlayerIsInsideAnyCollider()
    {
        foreach (Collider collider in spawnColliders)
        {
            if (collider.bounds.Contains(Player.transform.position))
            {
                return true;
            }
        }
        return false;
    }

    private IEnumerator SpawnZombies()
    {
        while (currentZombies < maxZombies)
        {
            if (canSpawn)
            {
                SpawnZombie();
                currentZombies++;
            }

            yield return new WaitForSeconds(spawnCooldown);
        }

        yield return null; 

        while (currentZombies > 0)
        {
            yield return null;
        }

        canSpawn = false;
        StartCoroutine(WaitForPlayerEnter());
    }

    private void SpawnZombie()
    {
        /*  int randomSpawnIndex = Random.Range(0, spawnPoints.Length);
          Instantiate(zombiePrefab, spawnPoints[randomSpawnIndex].position, Quaternion.identity);*/
        int randomSpawnIndex = Random.Range(0, spawnColliders.Count);
        Collider spawnCollider = spawnColliders[randomSpawnIndex];
        Vector3 randomPosition = GetRandomPositionInCollider(spawnCollider);
        Instantiate(zombiePrefab, randomPosition, Quaternion.identity);
    }
    private Vector3 GetRandomPositionInCollider(Collider collider)
    {
        Bounds bounds = collider.bounds;
        Vector3 randomPosition = new Vector3(
            Random.Range(bounds.min.x, bounds.max.x),
            Random.Range(bounds.min.y, bounds.max.y),
            Random.Range(bounds.min.z, bounds.max.z)
        );
        return randomPosition;
    }
}   



