using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieSpawn : MonoBehaviour
{
    public int numZombiesToKillBeforeSpawn = 10;
    private int numZombiesKilled = 0;
    private float spawnCooldown = 5f;


    private void Start()
    {
        Invoke("SpawnZombie", spawnCooldown);
    }

    private void Update()
    {
        if (numZombiesKilled >= numZombiesToKillBeforeSpawn)
        {
            Invoke("SpawnZombie", spawnCooldown);
            numZombiesKilled = 0;
        }


    }

    private void SpawnZombie()
    {
        Vector3 spawnPos = transform.position;
        Instantiate(GameObject.FindGameObjectWithTag("Zombie"), spawnPos, Quaternion.identity);

    }

    public void ZombieKilled()
    {
        numZombiesKilled++;
    }

}
