using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.UI;

public class Weapon : SlotController
{
    public WeaponDataSO weaponData;
    private AudioSource _audioSource;
    public Transform freePoint;
    public Text textBullets;
    public Text textRecharge;

    private float _bulletHitMissDistance = 120f;
    [SerializeField]
    private Transform _bulletParent;
    private Transform _mainCamera;
    public GameObject muzzleFlash;

    private bool _canShoot = true;

    private float _timeSinceLastShot;
    public float range = 100f;


    public override void OnChange()
    {
        throw new System.NotImplementedException();
    }

    public override void OnAwake()
    {
        _mainCamera = Camera.main.transform;
        _audioSource = GetComponent<AudioSource>();
    }

    public override void OnUpdate()
    {
        _timeSinceLastShot += Time.deltaTime;

        if (Input.GetMouseButton(0))
        {

            if (_canShoot && weaponData._currentAmmo > 0)
            {
                Shoot();
            }
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            Reload();
        }
    }

    public override void OnLateUpdate()
    {
        throw new System.NotImplementedException();
    }
    private void Shoot()
    {

        for (int i = 0; i < weaponData.NumberBulletsXShoot; i++)
        {
            RaycastHit hit;
            GameObject bullet = GameObject.Instantiate(weaponData.bulletPF, freePoint.position, weaponData.bulletPF.transform.rotation, _bulletParent);
            Instantiate(muzzleFlash, freePoint.position, Quaternion.identity);
            _audioSource.clip = AudioManagerClips.AudioManager.audioListSO.shootPistolPlayer;
            _audioSource.Play();
            Bullet bulletController = bullet.GetComponent<Bullet>();
            if (Physics.Raycast(_mainCamera.position, _mainCamera.forward, out hit, Mathf.Infinity))
            {
                bulletController.target = hit.point;
                bulletController.hit = true;
            }
            else
            {
                bulletController.target = _mainCamera.position + _mainCamera.forward * _bulletHitMissDistance;
                bulletController.hit = false;
            }
        }

        _canShoot = false;
        StartCoroutine(ShootCoolDown());
        weaponData._currentAmmo--;
        _timeSinceLastShot = 0f;
        textBullets.text = weaponData._currentAmmo.ToString() + " | " + weaponData._maxAmmo.ToString();
        if (weaponData._currentAmmo <= 0)
        {
            Reload();
        }
    }

    private void Reload()
    {
        textRecharge.text = "Recharging...";
        if (_timeSinceLastShot > weaponData.timeToRecharge && weaponData._currentAmmo < weaponData.MaxAmmoCarried)
        {
            _audioSource.clip = AudioManagerClips.AudioManager.audioListSO.rechargePistol;
            _audioSource.Play();
            weaponData._currentAmmo = weaponData._maxAmmoLoadder;
            weaponData._maxAmmo -= weaponData._maxAmmoLoadder;
            _timeSinceLastShot = 0;
            textRecharge.text = " ";
        }
    }
    public void AddAmmo()
    {
        weaponData._maxAmmo += 120;
    }
    private IEnumerator ShootCoolDown()
    {
        yield return new WaitForSeconds(0.5f);
        _canShoot = true;
    }
}