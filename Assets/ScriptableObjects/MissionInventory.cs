using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewMisionInventoryData", menuName = "MisionInventoryData", order = 2)]
public class MissionInventory : ScriptableObject
{
    public List<Mission> inventory = new List<Mission>();
}
