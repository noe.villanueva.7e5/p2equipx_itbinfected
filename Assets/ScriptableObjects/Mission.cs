using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewMisionData", menuName ="MisionData", order = 1)]
public class Mission : ScriptableObject
{
    public string MissionPanelText;
    public string TasksPanelText; 
    public List<GameObject> MissionItemPrefab;
    public List<GameObject> spawnPosition;
    public bool completed;
}
