using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemData : ScriptableObject
{
    [SerializeField]
    private string _name;
    public string Name { get { return _name; } }

    [SerializeField]
    private string _description;
    public string Description { get { return _description; } }

    public Sprite DefaultItemSprite;

    public GameObject prefabObject;

}